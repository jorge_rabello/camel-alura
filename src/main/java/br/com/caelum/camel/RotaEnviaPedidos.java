package br.com.caelum.camel;

import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;

public class RotaEnviaPedidos {

  public static void main(String[] args) throws Exception {

    DefaultCamelContext context = new DefaultCamelContext();
    context.addComponent("activemq", ActiveMQComponent.activeMQComponent("tcp://localhost:61616"));

    context.addRoutes(new RouteBuilder() {
      @Override
      public void configure() {
        from("file:pedidos?noop=true")
            .to("activemq:queue:pedidos");
      }
    });

    // inicializa o camel
    context.start();
    Thread.sleep(20000);
    context.stop();

  }

}
